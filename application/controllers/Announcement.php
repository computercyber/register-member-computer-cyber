<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Announcement extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_member_candidate');
        $this->load->model('M_configuration');
    }

    public function index()
    {
        $data['members'] = $this->M_member_candidate->memberAcc()->result();
        $data['berita'] = $this->M_member_candidate->newsAnnouncement()->result();
        $data['config'] = $this->M_member_candidate->getConfigAnnouncement()->result();
        $data['site_config'] = $this->M_configuration->listing()->result();

        foreach ($data['config'] as $config) {
            $start = $config->tanggal_mulai;
            $end = $config->tanggal_selesai;
        }
        $now = date("Y-m-d");

        // static date register
        // $start = date('2019-08-21'); 

        // pending bentar
        $this->load->view('layout/partials/v_header', $data);
        if (strtotime($now) < strtotime($start)) {
            $this->load->view('v_waiting_announcement');
        } elseif (strtotime($now) <= strtotime($end)) {
            $this->load->view('v_announcement', $data);
        } else {
            $this->load->view('v_announcement', $data);
        }
        $this->load->view('layout/partials/v_footer', $data);
    }

    // public function newsAnnouncement()
    // {
    //     $data['berita'] = $this->M_member_candidate->newsAnnouncement()->result();
    //     $this->load->view('layout/partials/v_header');
    //     $this->load->view('v_announcement', $data);
    //     $this->load->view('layout/partials/v_footer');
    // }
}
