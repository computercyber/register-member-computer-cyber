<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_member_candidate');
		$this->load->model('M_configuration');
	}

	public function index()
	{
		$data['config'] = $this->M_member_candidate->getConfigRegister()->result();
		$data['site_config'] = $this->M_configuration->listing()->result();
		$data['divisi'] = $this->db->get('divisi')->result();

		$this->form_validation->set_rules('nama', 'Nama', 'required', array('required' => 'Nama Harus diisi'));
		$this->form_validation->set_rules('nim', 'Nim', 'required|numeric', array('required' => 'Nim Harus diisi', 'numeric' => 'Nim harus berupa angka'));
		$this->form_validation->set_rules('email', 'E-mail', 'required', array('required' => 'E-mail Harus diisi'));
		$this->form_validation->set_rules('no_hp', 'No HP', 'required|numeric', array('required' => 'No Hp Harus diisi', 'numeric' => 'No Hp harus berupa angka'));
		$this->form_validation->set_rules('jenis_kelamin', 'Jenis Kelamin', 'required', array('required' => 'Jenis Kelamin Harus diisi'));
		$this->form_validation->set_rules('tanggal_lahir', 'Tanggal lahir', 'required', array('required' => 'Tanggal lahir Harus diisi'));
		$this->form_validation->set_rules('jurusan', 'Jurusan', 'required', array('required' => 'Program Studi Harus diisi'));
		$this->form_validation->set_rules('asal_sekolah', 'Asal Sekolah', 'required', array('required' => 'Asal Sekolah Harus diisi'));
		// $this->form_validation->set_rules('gambar', 'Profile Picture', 'required');
		$this->form_validation->set_rules('alamat', 'Alamat', 'required', array('required' => 'Alamat Harus diisi'));
		$this->form_validation->set_rules('alasan_masuk', 'Alasan', 'required', array('required' => 'Alasan Harus diisi'));
		$this->form_validation->set_rules('divisi', 'Divisi', 'required', array('required' => 'Divisi Harus diisi'));


		foreach ($data['config'] as $config) {
			$start = strtotime($config->tanggal_mulai);
			$end = strtotime($config->tanggal_selesai);
		}

		$getDate = date("Y-m-d");
		$now = strtotime($getDate);

		// static date register
		// $start = date('2019-08-21');

		// pending bentar
		if ($this->form_validation->run() == FALSE) {
			if ($now < $start) {
				$this->load->view('v_waiting_register', $data);
			} elseif ($now <= $end) {
				$this->load->view('layout/partials/v_header', $data);
				$this->load->view('v_home', $data);
				$this->load->view('layout/partials/v_footer', $data);
			} elseif ($now > $end) {
				$this->load->view('layout/partials/v_header', $data);
				$this->load->view('v_closed', $data);
				$this->load->view('layout/partials/v_footer', $data);
			}
		} else {
			// ambil data dari form pendaftaran
			$nama = $this->input->post('nama');
			$nim = $this->input->post('nim');
			$email = $this->input->post('email');
			$no_hp = $this->input->post('no_hp');
			$jenis_kelamin = $this->input->post('jenis_kelamin');
			$tanggal_lahir = $this->input->post('tanggal_lahir');
			$jurusan = $this->input->post('jurusan');
			$gambar_before = $_FILES['gambar']['name'];
			$gambar = str_replace(" ", "_", $gambar_before);
			$link = $this->input->post('link');
			$asal_sekolah = $this->input->post('asal_sekolah');
			$pengalaman_organisasi = $this->input->post('pengalaman_organisasi');
			$alamat = $this->input->post('alamat');
			$alasan_masuk = $this->input->post('alasan_masuk');
			$divisi = $this->input->post('divisi');

			// $gambar = $_FILES['gambar']['name'];

			// if ($gambar = '') { } else {

			// 	$config['upload_path']          = './uploads/img_member_candidate/';
			// 	$config['allowed_types'] 				= 'jpg|jpeg|png';
			// 	$config['max_size']             = 2000;

			// 	$this->load->library('upload', $config);

			// 	if (!$this->upload->do_upload('gambar')) {
			// 		print_r($this->upload->display_errors());
			// 	} else {
			// 		$gambar = $this->upload->data('file_name');
			// 	}
			// var_dump($gambar);
			// die;

			if ($gambar) {
				$config->upload_path          = './uploads/img_member_candidate/';
				$config->allowed_types        = 'jpg|jpeg|png';
				$config->max_size             = '10000';
				// $config['max_width']            = 1024;
				// $config['max_height']           = 768;

				$this->load->library('upload', $config);

				if (!$this->upload->do_upload('gambar')) {
					$data->error = $this->upload->display_errors();
					$this->load->view('layout/partials/v_header', $data);
					$this->load->view('v_home', $data);
					$this->load->view('layout/partials/v_footer', $data);
				}
			}


			$data = array(
				'nama' => $nama,
				'nim' => $nim,
				'email' => $email,
				'no_hp' => $no_hp,
				'jenis_kelamin' => $jenis_kelamin,
				'tanggal_lahir' => $tanggal_lahir,
				'jurusan' => $jurusan,
				'gambar' => $gambar,
				'link' => $link,
				'asal_sekolah' => $asal_sekolah,
				'pengalaman_organisasi' => $pengalaman_organisasi,
				'alamat' => $alamat,
				'alasan_masuk' => $alasan_masuk,
				'divisi' => $divisi
			);


			$this->M_member_candidate->addMember($data, 'calon_anggota');
			$this->session->set_flashdata("message", "<script>
			$(window).on('load', function() {
			  $('#successModal').modal('show');
			});
		  </script>"); /* flashdata untuk data berhasil */
			redirect('/');
		}
	}



	// logika salah
	// if (strtotime($now) <= strtotime($start)) {
	// 	$this->load->view('v_waiting');
	// } elseif (strtotime($now) >= strtotime($start)) {
	// $this->load->view('layout/partials/v_header', $data);
	// $this->load->view('v_home');
	// $this->load->view('layout/partials/v_footer', $data);
	// } elseif (strtotime($now) >= strtotime($end)) {
	// 	echo "pendaftaran sudah ditutup";
	// }


	public function check_nim()
	{
		$check_nim_from_input = $this->db->get_where('calon_anggota', array(
			'nim' => $this->input->post('nim')
		))->row();

		if ($check_nim_from_input) {
			echo json_encode(array(
				'status' 	=> 0,
				'nim'		=> $this->input->post('nim'),
				'message' 	=> 'sudah terdaftar'
			));
		} else {
			echo json_encode(array(
				'status' 	=> 1,
				'nim'		=> $this->input->post('nim'),
				'message' 	=> 'belum terdaftar'
			));
		}
	}

	function addMember()
	{
		$data['config'] = $this->M_member_candidate->getConfigRegister()->result();
		$data['site_config'] = $this->M_configuration->listing()->result();

		foreach ($data['config'] as $config) {
			$start = strtotime($config->tanggal_mulai);
			$end = strtotime($config->tanggal_selesai);
		}

		$getDate = date("Y-m-d");
		$now = strtotime($getDate);

		// static date register
		// $start = date('2019-08-21');

		$this->form_validation->set_rules('nama', 'Nama', 'required', array('required' => 'Nama Harus diisi'));
		$this->form_validation->set_rules('nim', 'Nim', 'required|numeric', array('required' => 'Nim Harus diisi', 'numeric' => 'Nim harus berupa angka'));
		$this->form_validation->set_rules('email', 'E-mail', 'required', array('required' => 'E-mail Harus diisi'));
		$this->form_validation->set_rules('no_hp', 'No HP', 'required|numeric', array('required' => 'No Hp Harus diisi', 'numeric' => 'No Hp harus berupa angka'));
		$this->form_validation->set_rules('jenis_kelamin', 'Jenis Kelamin', 'required', array('required' => 'Jenis Kelamin Harus diisi'));
		$this->form_validation->set_rules('tanggal_lahir', 'Tanggal lahir', 'required', array('required' => 'Tanggal lahir Harus diisi'));
		$this->form_validation->set_rules('jurusan', 'Jurusan', 'required', array('required' => 'Program Studi Harus diisi'));
		$this->form_validation->set_rules('asal_sekolah', 'Asal Sekolah', 'required', array('required' => 'Asal Sekolah Harus diisi'));
		// $this->form_validation->set_rules('gambar', 'Profile Picture', 'required');
		$this->form_validation->set_rules('alamat', 'Alamat', 'required', array('required' => 'Alamat Harus diisi'));
		$this->form_validation->set_rules('alasan_masuk', 'Alasan', 'required', array('required' => 'Alasan Harus diisi'));
		$this->form_validation->set_rules('divisi', 'Divisi', 'required', array('required' => 'Divisi Harus diisi'));


		if ($this->form_validation->run() == FALSE) {
			// pending bentar
			if ($now < $start) {
				$this->load->view('v_waiting_register', $data);
			} elseif ($now <= $end) {
				$this->load->view('layout/partials/v_header', $data);
				$this->load->view('v_home');
				$this->load->view('layout/partials/v_footer', $data);
			} elseif ($now > $end) {
				$this->load->view('layout/partials/v_header', $data);
				$this->load->view('v_closed', $data);
				$this->load->view('layout/partials/v_footer', $data);
			}
		} else {
			if ($gambar = '') {
			} else {

				// ambil data dari form pendaftaran
				$nama = $this->input->post('nama');
				$nim = $this->input->post('nim');
				$email = $this->input->post('email');
				$no_hp = $this->input->post('no_hp');
				$jenis_kelamin = $this->input->post('jenis_kelamin');
				$tanggal_lahir = $this->input->post('tanggal_lahir');
				$jurusan = $this->input->post('jurusan');
				$gambar = $_FILES['gambar']['name'];
				$link = $this->input->post('link');
				$asal_sekolah = $this->input->post('asal_sekolah');
				$pengalaman_organisasi = $this->input->post('pengalaman_organisasi');
				$alamat = $this->input->post('alamat');
				$alasan_masuk = $this->input->post('alasan_masuk');
				$divisi = $this->input->post('divisi');

				// $gambar = $_FILES['gambar']['name'];

				// if ($gambar = '') { } else {

				// 	$config['upload_path']          = './uploads/img_member_candidate/';
				// 	$config['allowed_types'] 				= 'jpg|jpeg|png';
				// 	$config['max_size']             = 2000;

				// 	$this->load->library('upload', $config);

				// 	if (!$this->upload->do_upload('gambar')) {
				// 		print_r($this->upload->display_errors());
				// 	} else {
				// 		$gambar = $this->upload->data('file_name');
				// 	}

				$config['upload_path']          = './uploads/img_member_candidate/';
				$config['allowed_types'] 		= 'jpg|jpeg|png';
				$config['max_size']             = 5000;

				$this->load->library('upload', $config);

				if (!$this->upload->do_upload('gambar')) {
					echo $this->upload->display_errors();
				} else {
					$gambar = $this->upload->data('file_name');
				}
			}

			$data = array(
				'nama' => $nama,
				'nim' => $nim,
				'email' => $email,
				'no_hp' => $no_hp,
				'jenis_kelamin' => $jenis_kelamin,
				'tanggal_lahir' => $tanggal_lahir,
				'jurusan' => $jurusan,
				'gambar' => $gambar,
				'link' => $link,
				'asal_sekolah' => $asal_sekolah,
				'pengalaman_organisasi' => $pengalaman_organisasi,
				'alamat' => $alamat,
				'alasan_masuk' => $alasan_masuk,
				'divisi' => $divisi
			);

			$this->M_member_candidate->addMember($data, 'calon_anggota');
			$this->session->set_flashdata("message", '<div class="alert alert-success alert-dismissible fade show" role="alert">Data telah terkirim! Nantikan informasi dari kami selanjutnya, terimakasih.
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
		</div>'); /* flashdata untuk data berhasil */
			redirect('/');
		}
		// else {
		// 	// $this->load->view('formsuccess');
		// 	echo "Data Cannot be Upload";
		// }



	}
}
