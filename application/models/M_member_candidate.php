<?php

class M_member_candidate extends CI_Model
{

    public function addMember($data, $table)
    {
        $this->db->insert($table, $data);
    }

    public function memberAcc()
    {
        return $this->db->get('anggota_diterima');
    }

    public function newsAnnouncement()
    {
        return $this->db->get('berita_anggota_diterima');
    }

    public function getConfigRegister()
    {
        return $this->db->get('konfigurasi_pendaftaran');
    }

    public function getConfigAnnouncement()
    {
        return $this->db->get('konfigurasi_pengumuman');
    }
}
