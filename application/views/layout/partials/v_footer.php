<style>
  .footer-title {
    font-family: 'Quicksand', sans-serif;
  }

  .footer-link {
    margin-bottom: -1px;
  }

  .footer-link a {
    color: black;
  }

  footer {
    font-family: 'Quicksand', sans-serif;
    font-size: 15px !important;
  }

  @media (min-width: 992px) {
    .sosmed {
      transition: .4s;
      color: #6B747C;
    }

    .sosmed:hover {
      color: #5E31C2;
    }
  }
</style>

<footer class="page-footer font-small unique-color-dark border-top">

  <div class="container text-center text-md-left mt-5 mb-4">
    <div class="row mt-3">

      <div class="col-md-3 col-lg-4 col-xl-3 mx-auto mb-4">
        <h6 class="text-uppercase font-weight-bold mb-3 footer-title">Address</h6>
        <p>Jl. Politeknik, KM. 24, Senggarang, Tanjung Pinang, Kota Tanjung Pinang, Kepulauan Riau 29115</p>
      </div>

      <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mb-4">
        <h6 class="text-uppercase font-weight-bold mb-3 footer-title">Help</h6>
        <p class="footer-link">
          <a href="https://umrah.ac.id" target="_blank">Bantuan</a>
        </p>
        <p class="footer-link">
          <a href="https://umrah.ac.id" target="_blank">Cara bergabung</a>
        </p>
        <p class="footer-link">
          <a href="https://umrah.ac.id" target="_blank">Pindah Divisi</a>
        </p>
        <p class="footer-link">
          <a href="https://umrah.ac.id" target="_blank">Jetskill</a>
        </p>
      </div>

      <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mb-4">
        <h6 class="text-uppercase font-weight-bold mb-3 footer-title">Website</h6>
        <p class="footer-link">
          <a href="https://umrah.ac.id" target="_blank">Jetskill</a>
        </p>
        <p class="footer-link">
          <a href="https://umrah.ac.id" target="_blank">Developer</a>
        </p>
        <p class="footer-link">
          <a href="https://umrah.ac.id" target="_blank">Recruitment</a>
        </p>
      </div>

      <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4">
        <h6 class="text-uppercase font-weight-bold mb-3 footer-title">Contact</h6>
        <p class="footer-link">
          <i class="fas fa-envelope mr-3 text-muted"></i>cc.umrah@gmail.com</p>
        <p class="footer-link">
          <i class="fas fa-phone mr-3 text-muted"></i>+6285658553529</p>
      </div>

    </div>

    <div class="footer-copyright py-3">
      <div class="row">

        <div class="col-md-3 col-lg-4 col-xl-3 mx-auto mb-4">
          <small class="text-small text-muted">&copy; <?php echo date("Y"); ?> Copyright Computer Cyber Study Club dikelola oleh Divisi Web Programming</small>
        </div>

        <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mb-4">

        </div>

        <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4">
          <a href="" target="_blank"><i class="fab fa-twitter sosmed"></i></a>
          <a href="" target="_blank"><i class="fab fa-instagram ml-4 sosmed"></i></a>
          <a href="" target="_blank"><i class="fab fa-facebook ml-4 sosmed"></i></a>
        </div>

      </div>
    </div>

  </div>

</footer>
<!-- Footer -->


<script>
  // Example starter JavaScript for disabling form submissions if there are invalid fields
  (function() {
    'use strict';
    window.addEventListener('load', function() {
      // Fetch all the forms we want to apply custom Bootstrap validation styles to
      var forms = document.getElementsByClassName('needs-validation');
      // Loop over them and prevent submission
      var validation = Array.prototype.filter.call(forms, function(form) {
        form.addEventListener('submit', function(event) {
          if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
          }
          form.classList.add('was-validated');
        }, false);
      });
    }, false);
  })();
</script>

<script>
  $(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip()
  });

  $('.custom-file-input').on('change', function() {
    let fileName = $(this).val().split('\\').pop();
    $(this).next('.custom-file-label').addClass("selected").html(fileName);
  });
</script>



<!-- Bootstrap core JavaScript -->
<script src="<?php echo base_url('assets'); ?>/vendor/jquery/jquery.min.js"></script>
<script src="<?php echo base_url('assets'); ?>/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Plugin JavaScript -->
<script src="<?php echo base_url('assets'); ?>/vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Contact form JavaScript -->
<script src="<?php echo base_url('assets'); ?>/js/jqBootstrapValidation.js"></script>
<script src="<?php echo base_url('assets'); ?>/js/contact_me.js"></script>

<!-- Custom scripts for this template -->
<script src="<?php echo base_url('assets'); ?>/js/agency.js"></script>

</body>

</html>