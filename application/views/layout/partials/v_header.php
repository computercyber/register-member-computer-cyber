<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php foreach ($site_config as $site_config) : ?>
        <link rel="shortcut icon" href="<?php echo base_url('assets/img/favicon/' . $site_config->logo); ?>">
        <meta name="description" content="<?php echo $site_config->namaweb ?>" />
        <meta name="keywords" content="<?php echo $site_config->namaweb ?>" />
        <meta name="author" content="<?php echo $site_config->namaweb ?>" />
    <?php endforeach; ?>

    <title><?php echo $site_config->namaweb ?> - Register</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url('assets'); ?>/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="<?php echo base_url('assets'); ?>/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,600,700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700&display=swap" rel="stylesheet">

    <!-- animate.css -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url('assets'); ?>/css/agency.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url('assets'); ?>/css/style.css" rel="stylesheet">

    <style>
        .font-quick-sand {
            font-family: 'Quicksand', sans-serif;
        }

        .font-open-sans {
            font-family: 'Open Sans', sans-serif;
        }

        .navbar2 {
            box-shadow: 1px 1px 2px 1px rgba(0, 0, 0, .1);
            background-color: white;
        }

        .navbar-toggler {
            font-family: 'Quicksand', sans-serif;
            font-size: 13px;
            text-transform: uppercase;
        }
    </style>

</head>

<body id="page-top">

    <?php if ($this->uri->segment(1) == "") { ?>

        <!-- Navigation -->
        <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
            <div class="container">
                <a class="navbar-brand js-scroll-trigger font-quick-sand" href="<?php echo site_url('/') ?>">Computer Cyber</a>
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation" style="color: #0E2A3E">
                    Menu
                    <i class="fas fa-bars" style="color: #0E2A3E"></i>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item mr-3">
                            <a class="nav-link js-scroll-trigger <?php if ($this->uri->segment(1) == "") {
                                                                        echo "active";
                                                                    } elseif ($this->uri->segment(1) == "home") {
                                                                        echo "active";
                                                                    } elseif ($this->uri->segment(2) == "addMember") {
                                                                        echo "active";
                                                                    } ?>" href="<?php echo site_url('/') ?>">Register</a>
                        </li>
                        <li class="nav-item mr-3">
                            <a class="nav-link js-scroll-trigger <?php if ($this->uri->segment(1) == "announcement") {
                                                                        echo "active";
                                                                    } ?>" href="<?php echo site_url('announcement') ?>">Announcement</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link js-scroll-trigger" href="http://computer-cyber.org" target="_blank">See Our Website</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

    <?php } else { ?>

        <!-- Navigation -->
        <nav class="navbar navbar2 navbar-expand-lg navbar-dark fixed-top">
            <div class="container">
                <a class="navbar-brand js-scroll-trigger font-quick-sand" href="<?php echo site_url('/') ?>">Computer Cyber</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation" style="color: #0E2A3E">
                    Menu
                    <i class="fas fa-bars" style="color: #0E2A3E"></i>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item mr-3">
                            <a class="nav-link js-scroll-trigger <?php if ($this->uri->segment(1) == "") {
                                                                        echo "active";
                                                                    } elseif ($this->uri->segment(1) == "home") {
                                                                        echo "active";
                                                                    } elseif ($this->uri->segment(2) == "addMember") {
                                                                        echo "active";
                                                                    } ?>" href="<?php echo site_url('/') ?>">Register</a>
                        </li>
                        <li class="nav-item mr-3">
                            <a class="nav-link js-scroll-trigger <?php if ($this->uri->segment(1) == "announcement") {
                                                                        echo "active";
                                                                    } ?>" href="<?php echo site_url('announcement') ?>">Announcement</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link js-scroll-trigger" href="http://computer-cyber.org" target="_blank">See Our Website</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

    <?php } ?>