<br>
<br>
<br>
<br>
<br>



<div class="container" style="font-family: 'Quicksand', sans-serif !important;">
    <div class="row">
        <div class="col-lg-10 offset-1">
            <?php foreach ($berita as $berita) : ?>
                <h4><?php echo $berita->judul . " "; ?><span class="badge badge-danger">NEW</span></h4>
            <?php endforeach; ?>
        </div>
    </div>

    <div class="row mt-4">
        <div class="col-md-10 offset-1">
            <p><?php echo $berita->pra_konten; ?></p>
        </div>
    </div>

    <div class="row mt-2">
        <div class="col-md-8 offset-md-2">
            <div class="table-responsive">
                <table class="table table-sm table-striped table-bordered">
                    <thead>
                        <tr style="color:#8D00DC">
                            <th scope="col" width="10">No</th>
                            <th scope="col">Nama</th>
                            <th scope="col">Nim</th>
                            <th scope="col">Divisi</th>
                            <th scope="col">Status</th>
                        </tr>
                    </thead>
                    <tbody style="font-size: 15px">
                        <?php
                        $no = 1;
                        foreach ($members as $member) : ?>
                            <tr>
                                <td scope="row"><?php echo $no++ ?>.</td>
                                <td><?php echo $member->nama; ?></td>
                                <td><?php echo $member->nim; ?></td>
                                <td><?php echo $member->divisi; ?></td>
                                <td><span class="badge badge-success">Diterima</span></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="row mt-2">
        <div class="col-md-10 offset-1">
            <p><?php echo $berita->post_konten; ?></p>
        </div>
    </div>



</div>