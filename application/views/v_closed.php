<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php foreach ($site_config as $site_config) : ?>
        <link rel="shortcut icon" href="<?php echo base_url('assets/img/favicon/' . $site_config->logo); ?>">
        <meta name="description" content="<?php echo $site_config->namaweb ?>" />
        <meta name="keywords" content="<?php echo $site_config->namaweb ?>" />
        <meta name="author" content="<?php echo $site_config->namaweb ?>" />
    <?php endforeach; ?>

    <title><?php echo $site_config->namaweb ?> - Register</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url('assets'); ?>/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="<?php echo base_url('assets'); ?>/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,600,700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600,700&display=swap" rel="stylesheet">

    <!-- animate.css -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">

    <!-- Bootstrap core JavaScript -->
    <script src="<?php echo base_url('assets'); ?>/vendor/jquery/jquery.min.js"></script>

    <style>
        body {
            /* border: 1px red solid; */
            font-family: 'Poppins', sans-serif;
        }

        * {
            /* border: 1px red solid; */
        }

        .page-section {}

        #sticky-footer {
            color: #8D00DC;
            flex-shrink: none;
        }

        #about {
            font-family: 'Quicksand', sans-serif !important;
        }

        .subheading {
            font-size: 16px;
            font-weight: 400;
        }

        .btn-link {
            color: #6771E6;
            font-weight: 500;
        }

        .icon-timeline {
            margin-top: 43px;
            margin-left: 6px;
        }

        .bg-registration {
            background-image: linear-gradient(to right, #6771E6, #5E31C2);
        }

        .title-jadwal {
            margin-top: -60px;
        }

        .btn-yes {
            width: 180px;
            font-size: 100%;
            border-radius: 5rem;
            letter-spacing: .1rem;
            font-weight: bold;
            padding: .7rem;
            transition: all 0.2s;
            box-shadow: 0px 0px 20px 0px rgba(94, 49, 194, .5);
            background-image: linear-gradient(to right, #6771E6, #5E31C2);
        }


        .btn-submit {
            width: 200px;
            font-size: 95%;
            border-radius: 5rem;
            letter-spacing: .1rem;
            font-weight: bold;
            padding: .7rem;
            transition: all 0.2s;
            box-shadow: 0px 0px 20px 0px rgba(94, 49, 194, .5);
            background-image: linear-gradient(to right, #6771E6, #5E31C2);
        }

        .text-primary {
            color: #5E31C2;
        }


        @media (max-width: 575.98px) {

            #about {
                margin-top: 50px;
            }

            .icon-timeline {
                margin-top: 15px;
                width: 50%;
            }

            .accordion {
                padding-bottom: 40px;
            }

            .title-faq {
                margin-top: 50px;
            }

            .timer {
                margin-top: 100px;
            }

            .text_main {
                color: #8D00DC;
                font-size: 28px;
            }

            .display-4 {
                font-size: 30px;
            }

            .waiting_img {
                position: absolute;
                display: block;
                width: 65%;
                right: 0;
                left: 69px;
                top: -60vh;
                bottom: 0;

                /* width: 100%;
                margin-top: 25%; */
            }

        }

        @media (min-width: 992px) {
            #about {
                margin-top: 100px;
                font-family: 'Quicksand', sans-serif !important;
            }

            .timeline-panel {
                margin-top: 50px;
            }

            .waiting_img {
                /* position: absolute;
            display: block;
            width: 65%;
            right: 0;
            left: 0;
            top: 50vh;
            bottom: 0;
            */
                width: 100%;
                margin-top: 25%;
            }

            .text_main {
                color: #8D00DC;
            }

            .timer {
                margin-top: 35vh;
            }
        }
    </style>

    <title>Computer Cyber &dash; Register</title>
</head>

<body>



    <!-- About -->
    <section class="page-section" id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">

                    <div class="title-jadwal text-center">
                        <h2 class="mb-10">JADWAL</h2>
                        <p>Jadwal pendaftaran anggota baru Computer Cyber 2019.</p>
                    </div>

                    <ul class="timeline mt-5">
                        <li>
                            <div class="timeline-image bg-registration">
                                <img class="img-fluid icon-timeline" src="<?php echo base_url('assets'); ?>/icon/resume.png" alt="">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h5>2 - 9 September 2019</h5>
                                    <h5 class="subheading">Pendaftaran online dimulai</h5>
                                </div>
                                <div class="timeline-body">
                                </div>
                            </div>
                        </li>
                        <li class="timeline-inverted">
                            <div class="timeline-image bg-registration">
                                <img class="img-fluid icon-timeline" src="<?php echo base_url('assets'); ?>/icon/interview.png" alt="">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h5>16 - 19 September 2019</h5>
                                    <h4 class="subheading">Interview calon anggota baru</h4>
                                </div>
                                <div class="timeline-body">
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="timeline-image bg-registration">
                                <img class="img-fluid icon-timeline" src="<?php echo base_url('assets'); ?>/icon/selection.png" alt="">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h5>20 - 24 September 2019</h5>
                                    <h4 class="subheading">Seleksi anggota oleh tim Computer Cyber</h4>
                                </div>
                                <div class="timeline-body">
                                </div>
                            </div>
                        </li>
                        <li class="timeline-inverted">
                            <div class="timeline-image bg-registration">
                                <img class="img-fluid icon-timeline" src="<?php echo base_url('assets'); ?>/icon/announcement.png" alt="">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h5>2 Oktober 2019</h5>
                                    <h4 class="subheading">Pengumuman anggota baru Computer Cyber</h4>
                                </div>
                                <div class="timeline-body">
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="timeline-image bg-registration">
                                <img class="img-fluid icon-timeline" src="<?php echo base_url('assets'); ?>/icon/welcome.png" alt="">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h5 style="color:#5E31C2;">Coming Soon!</h5>
                                    <h4 class="subheading">Penyambutan anggota baru Computer Cyber</h4>
                                </div>
                                <div class="timeline-body">
                                </div>
                            </div>
                        </li>
                        <li class="timeline-inverted">
                            <div class="timeline-image bg-registration">
                                <h4>Saatnya
                                    <br>Perjalananmu
                                    <br>Dimulai!</h4>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>


    <section class="page-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 text-center" style="height:100vh;">
                    <div class="timer">
                        <div class="display-4 oops" style="font-weight:600; color:#8D00DC;">
                            Sorry...
                        </div>
                        <div class="display-4" style="font-weight:600; color:#490078;">
                            <span id="days"></span>
                            <span id="hours"></span>
                            <span id="minutes"></span>
                            <span id="second"></span>
                        </div>
                        <h1 class="text_main">Registration is closed.</h1>
                    </div>
                </div>
                <div class="col-lg-6 text-center">
                    <img class="waiting_img" src="<?php echo base_url(); ?>assets/img/register_cc/register_closed.svg" alt="">
                </div>
            </div>
        </div>
    </section>

    <script src="<?php echo base_url('assets'); ?>/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url('assets'); ?>/vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Contact form JavaScript -->
    <script src="<?php echo base_url('assets'); ?>/js/jqBootstrapValidation.js"></script>
    <script src="<?php echo base_url('assets'); ?>/js/contact_me.js"></script>

    <!-- Custom scripts for this template -->
    <script src="<?php echo base_url('assets'); ?>/js/agency.js"></script>
</body>

</html>