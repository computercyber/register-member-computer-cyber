<style>
  #about {
    font-family: 'Quicksand', sans-serif !important;
  }

  .subheading {
    font-size: 16px;
    font-weight: 400;
  }

  .btn {
    font-family: 'Quicksand', sans-serif !important;
  }

  .btn-link {
    color: #6771E6;
    font-weight: 500;
  }

  .icon-timeline {
    margin-top: 43px;
    margin-left: 6px;
  }

  .bg-registration {
    background-image: linear-gradient(to right, #6771E6, #5E31C2);
  }

  .title-jadwal {
    margin-top: -60px;
  }

  .btn-yes {
    width: 180px;
    font-size: 100%;
    border-radius: 5rem;
    letter-spacing: .1rem;
    font-weight: bold;
    padding: .7rem;
    transition: all 0.2s;
    box-shadow: 0px 0px 20px 0px rgba(94, 49, 194, .5);
    background-image: linear-gradient(to right, #6771E6, #5E31C2);
  }


  .btn-submit {
    width: 200px;
    font-size: 95%;
    border-radius: 5rem;
    letter-spacing: .1rem;
    font-weight: bold;
    padding: .7rem;
    transition: all 0.2s;
    box-shadow: 0px 0px 20px 0px rgba(94, 49, 194, .5);
    background-image: linear-gradient(to right, #6771E6, #5E31C2);
  }

  .text-primary {
    color: #5E31C2;
  }

  .modal-title {
    font-family: 'Quicksand', sans-serif !important;
  }

  @media (max-width: 575.98px) {
    .icon-timeline {
      margin-top: 15px;
      width: 50%;
    }

    .accordion {
      padding-bottom: 40px;
    }

    .title-faq {
      margin-top: 50px;
    }

  }

  @media (min-width: 992px) {
    .timeline-panel {
      margin-top: 50px;
    }
  }
</style>

<div class="jumbotron jumbotron-fluid jumbotron-home">
  <div class="container">
    <div class="row">
      <div class="col-lg-6">
        <div class="intro-text intro-text-home text-white">
          <div class="lead lead-home animated fadeIn delay-1s" style="text-align: left;">Nice to meet you!</div>
          <div class="display-4 display-4-home" style="text-align: left;">We invite you for</div>
          <div class="display-4 display-4-home2 text-uppercase" style="text-align: left;">Join our community</div>
          <a class="btn btn-primary btn-lg js-scroll-trigger btn-welcome mt-4 btn-yes border-0 text-white" href="#registration">Yes, I do</a>
        </div>
      </div>
      <div class="col-lg-6">
        <img class="people-jumbotron" src="<?php echo base_url() ?>assets/img/register_cc/people-main.png" alt="">
      </div>
    </div>
    <div class="row">
      <img class="bg-jumbotron" src="<?php echo base_url() ?>assets/img/register_cc/bg-main.png" alt="">
    </div>
  </div>
</div>

<div class="container">
  <div class="row mt-4">
    <div class="col-md-12">
      <?php echo $this->session->flashdata('message'); ?>
      <!-- flashdata untuk data berhasil -->
    </div>
  </div>
</div>

<!--  -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

<!-- About -->
<section class="page-section" id="about">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">

        <div class="title-jadwal text-center">
          <h2 class="mb-10">JADWAL</h2>
          <p>Jadwal pendaftaran anggota baru Computer Cyber 2019.</p>
        </div>

        <ul class="timeline mt-5">
          <li>
            <div class="timeline-image bg-registration">
              <img class="img-fluid icon-timeline" src="<?php echo base_url('assets'); ?>/icon/resume.png" alt="">
            </div>
            <div class="timeline-panel">
              <div class="timeline-heading">
                <h5>2 - 9 September 2019</h5>
                <h5 class="subheading">Pendaftaran online dimulai</h5>
              </div>
              <div class="timeline-body">
              </div>
            </div>
          </li>
          <li class="timeline-inverted">
            <div class="timeline-image bg-registration">
              <img class="img-fluid icon-timeline" src="<?php echo base_url('assets'); ?>/icon/interview.png" alt="">
            </div>
            <div class="timeline-panel">
              <div class="timeline-heading">
                <h5>16 - 19 September 2019</h5>
                <h4 class="subheading">Interview calon anggota baru</h4>
              </div>
              <div class="timeline-body">
              </div>
            </div>
          </li>
          <li>
            <div class="timeline-image bg-registration">
              <img class="img-fluid icon-timeline" src="<?php echo base_url('assets'); ?>/icon/selection.png" alt="">
            </div>
            <div class="timeline-panel">
              <div class="timeline-heading">
                <h5>20 - 24 September 2019</h5>
                <h4 class="subheading">Seleksi anggota oleh tim Computer Cyber</h4>
              </div>
              <div class="timeline-body">
              </div>
            </div>
          </li>
          <li class="timeline-inverted">
            <div class="timeline-image bg-registration">
              <img class="img-fluid icon-timeline" src="<?php echo base_url('assets'); ?>/icon/announcement.png" alt="">
            </div>
            <div class="timeline-panel">
              <div class="timeline-heading">
                <h5>25 September</h5>
                <h4 class="subheading">Pengumuman anggota baru Computer Cyber</h4>
              </div>
              <div class="timeline-body">
              </div>
            </div>
          </li>
          <li>
            <div class="timeline-image bg-registration">
              <img class="img-fluid icon-timeline" src="<?php echo base_url('assets'); ?>/icon/welcome.png" alt="">
            </div>
            <div class="timeline-panel">
              <div class="timeline-heading">
                <h5 style="color:#5E31C2;">Coming Soon!</h5>
                <h4 class="subheading">Penyambutan anggota baru Computer Cyber</h4>
              </div>
              <div class="timeline-body">
              </div>
            </div>
          </li>
          <li class="timeline-inverted">
            <div class="timeline-image bg-registration">
              <h4>Saatnya
                <br>Perjalananmu
                <br>Dimulai!</h4>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </div>
</section>

<section class="faq" id="faq" style="background-color:#FAFAFA;">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="title-faq text-center">
          <h2 class="mb-10">FAQ</h2>
          <p>Beberapa hal yang sering ditanyakan.</p>
        </div>

        <div class="accordion mt-5" id="accordion">

          <div class="card">
            <div class="card-header" id="headingOne" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              <h2 class="mb-0">
                <button class="btn btn-link">
                  Apa itu Computer Cyber?
                </button>
              </h2>
            </div>
            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
              <div class="card-body">
                Computer Cyber adalah organisasi dibawah naungan jurusan Teknik Informatika Universitas Maritim Raja Ali Haji berbasis Teknologi.
              </div>
            </div>
          </div>

          <div class="card">
            <div class="card-header" id="headingTwo" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
              <h2 class="mb-0">
                <button class="btn btn-link collapsed">
                  Apa Visi Computer Cyber?
                </button>
              </h2>
            </div>
            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
              <div class="card-body">
                Computer Cyber mempunyai visi yaitu <strong>"Menjadi Komunitas Universitas berbasis Teknologi"</strong>.
              </div>
            </div>
          </div>

          <div class="card">
            <div class="card-header" id="headingThree" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
              <h2 class="mb-0">
                <button class="btn btn-link collapsed">
                  Apa syarat untuk bergabung di Computer Cyber?
                </button>
              </h2>
            </div>
            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
              <div class="card-body">
                Adapun syarat untuk bergabung di Computer Cyber yaitu :
                <ul>
                  <li>Mahasiswa Universitas Maritim Raja Ali</li>
                  <li>Memiliki <strong>Keinginan</strong> dan <strong>Komitmen</strong> untuk ingin bergerak maju mengikuti perkembangan Teknologi</li>
                  <li>Memiliki jiwa kerjasama tim yang kuat (teamwork)</li>
                  <li>Suka menyelesaikan masalah (problem solving)</li>
                </ul>
              </div>
            </div>
          </div>

          <div class="card">
            <div class="card-header" id="headingFour" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
              <h2 class="mb-0">
                <button class="btn btn-link collapsed">
                  Divisi apa saja yang terdapat di Computer Cyber?
                </button>
              </h2>
            </div>
            <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionExample">
              <div class="card-body">
                Computer Cyber memiliki 5 divisi, yaitu :
                <ul>
                  <li><strong>Programming</strong></li>
                  <li><strong>Web Programming</strong></li>
                  <li><strong>Networking</strong></li>
                  <li><strong>Robotik</strong></li>
                  <li><strong>Multimedia</strong></li>
                </ul>
                Penjelasan terkait ke-5 divisi tersebut dapat dilihat di website <strong><a href="http://www.computer-cyber.org/divisi">Computer Cyber</a></strong> atau Contact Person pendaftaran kami.
              </div>
            </div>
          </div>

          <div class="card">
            <div class="card-header" id="headingSeven" type="button" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
              <h2 class="mb-0">
                <button class="btn btn-link collapsed">
                  Apakah saya dapat masuk ke lebih dari 1 divisi?
                </button>
              </h2>
            </div>
            <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordionExample">
              <div class="card-body">
                Tidak! Setiap anggota hanya dapat masuk satu divisi saja.
              </div>
            </div>
          </div>

          <div class="card">
            <div class="card-header" id="headingFive" type="button" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
              <h2 class="mb-0">
                <button class="btn btn-link collapsed">
                  Kapan dan dimana pendaftaran Computer Cyber dimulai?
                </button>
              </h2>
            </div>
            <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordionExample">
              <div class="card-body">
                Computer Cyber membuka pendaftaran bagi anggota baru tahun 2019 pada tanggal <strong>2 - 9 September 2019</strong>. Pendaftaran dapat di lakukan di website <strong><a href="http://www.register.computer-cyber.org">Register ini</a></strong>.
              </div>
            </div>
          </div>

          <div class="card">
            <div class="card-header" id="headingSix" type="button" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
              <h2 class="mb-0">
                <button class="btn btn-link collapsed">
                  Kapan dan dimana saya dapat melihat pengumuman anggota baru Computer Cyber yang diterima?
                </button>
              </h2>
            </div>
            <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordionExample">
              <div class="card-body">
                Kamu dapat melihat pengumuman anggota baru Computer Cyber yang diterima pada tanggal <strong>25 September 2019</strong>, dan pengumuman anggota yang diterima dapat dilihat di website ini pada laman <strong><a href="http://www.register.computer-cyber.org/announcement">Announcement</a></strong>.
              </div>
            </div>
          </div>

          <div class="card">
            <div class="card-header" id="headingEight" type="button" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
              <h2 class="mb-0">
                <button class="btn btn-link collapsed">
                  Apakah setelah saya mendaftar langsung diterima?
                </button>
              </h2>
            </div>
            <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordionExample">
              <div class="card-body">
                Tidak! Pertama calon anggota akan melewati proses <strong>interview</strong>, kemudian akan dilakukan <strong>seleksi</strong> oleh tim Computer Cyber, dan barulah <strong>pengumuman</strong> akan dipublish di website ini pada laman <strong><a href="http://www.register.computer-cyber.org/announcement">Announcement</a></strong>.
              </div>
            </div>
          </div>

          <div class="card">
            <div class="card-header" id="headingNine" type="button" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
              <h2 class="mb-0">
                <button class="btn btn-link collapsed">
                  Portfolio apakah yang harus saya kirimkan pada form pendaftaran?
                </button>
              </h2>
            </div>
            <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordionExample">
              <div class="card-body">
                Kamu dapat mengirimkan semua karya dan prestasi yang kamu miliki, seperti piagam, pernah mengikuti lomba, pernah membuat suatu karya, dokumentasi kegiatan, dll. Portfolio bisa dibidang IT ataupun Non IT. Portfolio yang kamu kirimkan harus berupa link <strong>Google Drive</strong> dan pastikan link yang kamu kirim <u>dapat diakses</u> secara <strong>publik</strong>.
              </div>
            </div>
          </div>

          <div class="card">
            <div class="card-header" id="headingTen" type="button" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
              <h2 class="mb-0">
                <button class="btn btn-link collapsed">
                  Seberapa pentingkah portfolio untuk saya kirimkan?
                </button>
              </h2>
            </div>
            <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordionExample">
              <div class="card-body">
                Portfolio sangat penting bagi kami untuk melakukan penilaian terhadap calon anggota baru Computer Cyber.
              </div>
            </div>
          </div>

          <div class="card">
            <div class="card-header" id="headingEleven" type="button" data-toggle="collapse" data-target="#collapseEleven" aria-expanded="false" aria-controls="collapseEleven">
              <h2 class="mb-0">
                <button class="btn btn-link collapsed">
                  Baiklah saya tertarik, lalu bagaimana cara saya mendaftar?
                </button>
              </h2>
            </div>
            <div id="collapseEleven" class="collapse" aria-labelledby="headingEleven" data-parent="#accordionExample">
              <div class="card-body">
                Cara daftar Computer Cyber sangat mudah, yaitu :
                <ul>
                  <li>Pertama kamu harus follow <strong><a href="https://www.instagram.com/computercyber/" target="_blank">Instagram Official Computer Cyber</a></strong> dan <strong><a href="https://twitter.com/ccumrah" target="_blank">Twitter Official Computer Cyber</a></strong> (jika punya twitter), agar tau info tentang Computer Cyber</li>
                  <li>Mengisi form pendaftaran dengan data yang valid</li>
                  <li>Pantengin terus social media Computer Cyber agar tidak ketinggalan info</li>
                </ul>
                Jika masih ada kesulitan atau ada hal yang ingin ditanyakan hubungi <strong>Contact Person</strong>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
</section>

<script>
  $('.accordion').collapse()
</script>

<?php if ($this->form_validation->run() == FALSE && validation_errors('<div class="alert alert-warning">', '</div>') != null) { ?>
  <script type="text/javascript">
    $(window).on('load', function() {
      $('#warningModal').modal('show');
    });
  </script>
<?php } elseif (isset($error)) { ?>
  <script type="text/javascript">
    $(window).on('load', function() {
      $('#warningProfileModal').modal('show');
    });
  </script>
<?php } ?>

<?php echo $this->session->flashdata('message'); ?>

<!-- Warning Modal -->
<div class="modal fade" id="warningModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Peringatan</h5>
      </div>
      <div class="modal-body">
        <div class="alert alert-danger" role="alert">
          Data belum lengkap! cek kembali form isian anda.
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!-- Success Modal -->
<div class="modal fade" id="successModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Berhasil</h5>
      </div>
      <div class="modal-body">
        <div class="alert alert-success" role="alert">
          Pendaftaran berhasil! Nantikan informasi dari kami selanjutnya, terimakasih.
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<section class="testimonial py-5" id="registration">
  <div class="container">
    <div class="row ">
      <div class="col-md-6 py-5 text-white text-center bg-registration">
        <div class=" ">
          <div class="card-body">
            <img src="http://www.ansonika.com/mavia/img/registration_bg.svg" style="width:30%">
            <h2 class="py-3">Form Registrasi Anggota Baru Computer Cyber</h2>
            <p>"Menjadi Komunitas Universitas berbasis Teknologi"</p>
          </div>
        </div>
      </div>
      <div class="col-md-6 py-5 border">
        <h4 class="pb-4 ml-2">Isi form ini dibawah ini</h4>
        <form class="ml-2 mr-2 needs-validation" action="<?php echo site_url('home'); ?>" method="POST" enctype="multipart/form-data" novalidate>
          <div class="form-group">
            <label for="nama">Nama lengkap</label>

            <input type="text" class="form-control <?php if ($this->form_validation->run() == FALSE && form_error('nama') != null) {
                                                      echo "is-invalid";
                                                    } ?>" id="nama" name="nama" placeholder="Nama lengkap" value="<?php echo set_value('nama'); ?>">
            <?php echo form_error('nama', '<div class="text-danger small mt-2">', '!</div>'); ?>
          </div>

          <div class="form-group">
            <label for="nim">NIM</label>
            <input type="text" class="form-control <?php if ($this->form_validation->run() == FALSE && form_error('nim') != null) {
                                                      echo "is-invalid";
                                                    } ?>" id="nim" name="nim" placeholder="1701552010..." value="<?php echo set_value('nim'); ?>">
            <?php echo form_error('nim', '<div class="text-danger small mt-2">', '!</div>'); ?>
            <small id="nim-status" class="form-text text-danger d-non"></small>
            <!-- <div id="nim-status"></div> -->
          </div>

          <div class="form-group">
            <label for="email">Alamat E-mail</label>
            <input type="email" class="form-control <?php if ($this->form_validation->run() == FALSE && form_error('email') != null) {
                                                      echo "is-invalid";
                                                    } ?>" id="email" name="email" aria-describedby="emailHelp" placeholder="emailkamu@domain.com" value="<?php echo set_value('email'); ?>">
            <?php echo form_error('email', '<div class="text-danger small mt-2">', '!</div>'); ?>
            <small id="emailHelp" class="form-text text-muted">Kami tidak akan memberikan email kamu kepada siapapun.</small>
          </div>

          <div class="form-group">
            <label for="no_hp">No Hp (WhatsApp)</label>
            <input type="text" class="form-control <?php if ($this->form_validation->run() == FALSE && form_error('no_hp') != null) {
                                                      echo "is-invalid";
                                                    } ?>" id="no_hp" name="no_hp" placeholder="+628..." value="<?php echo set_value('no_hp'); ?>">
            <?php echo form_error('no_hp', '<div class="text-danger small mt-2">', '!</div>'); ?>
          </div>

          <div class="form-group">
            <label for="genre" class=" <?php if ($this->form_validation->run() == FALSE && form_error('jenis_kelamin') != null) {
                                          echo "text-danger";
                                        } ?>">Jenis Kelamin</label>
            <div class="form-check">
              <input class="form-check-input" type="radio" name="jenis_kelamin" id="exampleRadios1" value="pria" <?php echo set_value('jenis_kelamin') == 'pria' ? "checked" : null ?>>
              <label class="ml-1 form-check-label" for="exampleRadios1">
                Pria
              </label>
            </div>
            <div class="form-check">
              <input class="form-check-input" type="radio" name="jenis_kelamin" id="exampleRadios2" value="wanita" <?php echo set_value('jenis_kelamin') == 'wanita' ? "checked" : null ?>>
              <label class="ml-1 form-check-label" for="exampleRadios2">
                Wanita
              </label>
            </div>
            <div class="form-check">
              <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios3" value="option3" disabled>
              <label class="form-check-label" for="exampleRadios3">
                Bukan keduanya
              </label>
            </div>
            <?php echo form_error('jenis_kelamin', '<div class="text-danger small mt-2">', '!</div>'); ?>
          </div>

          <div class="form-group">
            <label for="tanggal_lahir">Tanggal lahir</label>
            <input type="date" class="form-control <?php if ($this->form_validation->run() == FALSE && form_error('tanggal_lahir') != null) {
                                                      echo "is-invalid";
                                                    } ?>" name="tanggal_lahir" id="tanggal_lahir" value="<?php echo set_value('tanggal_lahir'); ?>">
            <?php echo form_error('tanggal_lahir', '<div class="text-danger small mt-2">', '!</div>'); ?>
          </div>

          <div class="form-group">
            <label for="jurusan">Program Studi</label>
            <select class="form-control" required name="jurusan">
              <option selected disabled>-- Jurusan --</option>
              <optgroup label="Teknik">
                <option value="1" <?php echo set_value('jurusan') == 1 ? "selected" : null ?>>Teknik Informatika</option>
                <option value="2" <?php echo set_value('jurusan') == 2 ? "selected" : null ?>>Teknik Elektro</option>
              </optgroup>
              <optgroup label="Ekonomi">
                <option value="3" <?php echo set_value('jurusan') == 3 ? "selected" : null ?>>Akuntansi</option>
                <option value="4" <?php echo set_value('jurusan') == 4 ? "selected" : null ?>>Manajemen</option>
              </optgroup>
              <optgroup label="Fakultas Ilmu Kelautan dan Perikanan">
                <option value="5" <?php echo set_value('jurusan') == 5 ? "selected" : null ?>>Ilmu Kelautan</option>
                <option value="6" <?php echo set_value('jurusan') == 6 ? "selected" : null ?>>Manajemen Sumberdaya Perairan</option>
                <option value="7" <?php echo set_value('jurusan') == 7 ? "selected" : null ?>>Budidaya Perairan</option>
                <option value="8" <?php echo set_value('jurusan') == 8 ? "selected" : null ?>>Teknologi Hasil Perikanan</option>
                <option value="9" <?php echo set_value('jurusan') == 9 ? "selected" : null ?>>Sosial Ekonomi Perikanan</option>
              </optgroup>
              <optgroup label="Fakultas Keguruan dan Ilmu Pendidikan">
                <option value="10" <?php echo set_value('jurusan') == 10 ? "selected" : null ?>>Pendidikan Bahasa dan Sastra Indonesia</option>
                <option value="11" <?php echo set_value('jurusan') == 11 ? "selected" : null ?>>Pendidikan Bahasa Inggris</option>
                <option value="12" <?php echo set_value('jurusan') == 12 ? "selected" : null ?>>Pendidikan Kimia</option>
                <option value="13" <?php echo set_value('jurusan') == 13 ? "selected" : null ?>>Pendidikan Biologi</option>
                <option value="14" <?php echo set_value('jurusan') == 14 ? "selected" : null ?>>Pendidikan Matematika</option>
              </optgroup>
              <optgroup label="Fakultas Ilmu Sosial dan Ilmu Politik">
                <option value="15" <?php echo set_value('jurusan') == 15 ? "selected" : null ?>>Imu Administrasi Negara</option>
                <option value="16" <?php echo set_value('jurusan') == 16 ? "selected" : null ?>>Ilmu Pemerintahan</option>
                <option value="17" <?php echo set_value('jurusan') == 17 ? "selected" : null ?>>Sosiologi</option>
                <option value="18" <?php echo set_value('jurusan') == 18 ? "selected" : null ?>>Ilmu Hukum</option>
                <option value="19" <?php echo set_value('jurusan') == 19 ? "selected" : null ?>>Hubungan Internasional</option>
              </optgroup>
            </select>
            <!-- <input type="text" class="form-control <?php if ($this->form_validation->run() == FALSE && form_error('jurusan') != null) {
                                                          echo "is-invalid";
                                                        } ?>" name="jurusan" id="jurusan" placeholder="cth. Teknik Informatika" value="<?php echo set_value('jurusan'); ?>"> -->
            <?php echo form_error('jurusan', '<div class="text-danger small mt-2">', '!</div>'); ?>
          </div>

          <div class="form-group">
            <label for="gambar">Foto Profile</label>
            <i class="ml-1 fas fa-info-circle text-primary" data-toggle="tooltip" data-placement="right" title="Usahakan memilih gambar dengan latar belakang yang bersih">
            </i>
            <div class="input-group input-group-lg" style="margin-bottom:15px;">
              <input name="gambar" class="custom-file-input <?php if (isset($error)) {
                                                              echo "is-invalid";
                                                            } ?>" type="file" class="mt-3" id="gambar">
              <label for="gambar" class="custom-file-label text-muted">Pilih atau seret gambar ...</label>
              <?php if (isset($error)) { ?>
                <?php echo $error ?>
              <?php } ?>
            </div>

            <div class="form-group">
              <label for="link">Link Portfolio</label>
              <small class="ml-1" data-toggle="tooltip" data-placement="right" title="Jika kamu memiliki portfolio. Itu akan menjadi pertimbangan kami dalam menyeleksi anggota" style="color:#5E31C2;"><strong>Kenapa saya harus menyertakan portfolio?</strong></small>
              <input type="text" class="form-control" name="link" id="link" placeholder="Hanya Google Drive" value="<?php echo set_value('link'); ?>">
              <small id="emailHelp" class="form-text text-muted">Kamu dapat mengirimkan semua karya dan prestasi yang kamu miliki, seperti piagam, pernah mengikuti lomba, pernah membuat suatu karya, dokumentasi kegiatan, dll. Portfolio bisa dibidang IT ataupun Non IT. Portfolio yang kamu kirimkan harus berupa link <strong>Google Drive</strong> dan pastikan link yang kamu kirim <u>dapat diakses</u> secara <strong>publik</strong>.</small>
            </div>

            <div class="form-group">
              <label for="asal_sekolah">Asal Sekolah</label>
              <input type="text" class="form-control <?php if ($this->form_validation->run() == FALSE && form_error('asal_sekolah') != null) {
                                                        echo "is-invalid";
                                                      } ?>" id="asal_sekolah" name="asal_sekolah" placeholder="Cth. SMAN 1 Toayapa" value="<?php echo set_value('asal_sekolah'); ?>">
              <?php echo form_error('asal_sekolah', '<div class="text-danger small mt-2">', '!</div>'); ?>
            </div>

            <div class="form-group">
              <label for="pengalaman_organisasi">Pengalaman Organisasi</label>
              <textarea class="form-control" id="pengalaman_organisasi" name="pengalaman_organisasi" rows="3" placeholder="cth. Ketua OSIS"><?php echo set_value('pengalaman organisasi'); ?></textarea>
              <small id="emailHelp" class="form-text text-muted">Isi semua pengalaman organisasi yang kamu punya.</small>
            </div>

            <div class="form-group">
              <label for="alamat">Alamat</label>
              <textarea class="form-control <?php if ($this->form_validation->run() == FALSE && form_error('alamat') != null) {
                                              echo "is-invalid";
                                            } ?>" id="alamat" name="alamat" rows="3" placeholder="Alamat di Tanjungpinang"><?php echo set_value('alamat'); ?></textarea>
              <?php echo form_error('alamat', '<div class="text-danger small mt-2">', '!</div>'); ?>
            </div>

            <div class="form-group">
              <label for="alasan_masuk">Alasan ingin bergabung di Computer Cyber</label>
              <textarea class="form-control <?php if ($this->form_validation->run() == FALSE && form_error('alasan_masuk') != null) {
                                              echo "is-invalid";
                                            } ?>" id="alasan_masuk" name="alasan_masuk" rows="4" placeholder="Tulis alasanmu dengan detail"><?php echo set_value('alasan_masuk'); ?></textarea>
              <?php echo form_error('alasan_masuk', '<div class="text-danger small mt-2">', '!</div>'); ?>
            </div>

            <div class="form-group">
              <label for="divisi">Divisi yang kamu pilih</label>
              <select class="form-control <?php if ($this->form_validation->run() == FALSE && form_error('divisi') != null) {
                                            echo "is-invalid";
                                          } ?>" id="divisi" name="divisi">
                <option disabled selected>Pilih divisi</option>
                <?php foreach ($divisi as $divisi) : ?>
                  <option value="<?php echo $divisi->nama_divisi; ?>" <?php echo set_value('divisi') == $divisi->nama_divisi ? "selected" : null ?>><?php echo $divisi->nama_divisi; ?></option>
                <?php endforeach ?>
              </select>
              <?php echo form_error('divisi', '<div class="text-danger small mt-2">', '!</div>'); ?>
              <small id="emailHelp" class="form-text text-muted">Pemilihan divisi pada form ini bukan merupakan keputusan final, keputusan final akan ditanyakan kembali pada saat <strong>interview</strong>.</small>
            </div>

            <div class="form-group">
              <div class="form-check">
                <input class="form-check-input" type="checkbox" value="" id="invalidCheck" required>
                <label class="form-check-label ml-2" for="invalidCheck">
                  <strong>Saya setuju terhadap semua peraturan yang berlaku</strong>
                </label>
                <div class="ml-2 invalid-feedback">
                  Anda harus setuju sebelum mengirimkan data
                </div>
              </div>
            </div>

            <div class="form-group">
              <button type="submit" class="btn btn-primary btn-submit border-0">Daftar sekarang</button>
            </div>
        </form>
      </div>
    </div>
  </div>
</section>

<script>
  $('#nim').on('keyup', function() {

    const keyword = $(this).val();

    $.ajax({
      url: '<?php echo site_url('home/check_nim'); ?>',
      data: {
        nim: keyword
      },
      method: 'POST',
      dataType: 'JSON',
      success: function(data) {
        if (data.status == 1) {
          $('#nim-status').html('NIM ' + data.nim + ' ' + data.message);
          $('#nim').removeClass('is-invalid');
          $('#nim-status').addClass('d-none');
          console.log(data.status);
        } else {
          $('#nim-status').removeClass('d-none');
          $('#nim-status').html('NIM ' + data.nim + ' ' + data.message);
          $('#nim').addClass('is-invalid');
          console.log(data.status);
        }
      }
    });
  });
</script>