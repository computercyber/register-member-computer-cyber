<section class="page-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-10 offset-1">
                <div class="alert alert-warning" role="alert">
                    Belum ada pengumuman tersedia.
                </div>
            </div>
        </div>
    </div>
</section>