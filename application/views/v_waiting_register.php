<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php foreach ($site_config as $site_config) : ?>
    <link rel="shortcut icon" href="<?php echo base_url('assets/img/favicon/' . $site_config->logo); ?>">
    <meta name="description" content="<?php echo $site_config->namaweb ?>" />
    <meta name="keywords" content="<?php echo $site_config->namaweb ?>" />
    <meta name="author" content="<?php echo $site_config->namaweb ?>" />
    <?php endforeach; ?>

    <title><?php echo $site_config->namaweb ?> - Register</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url('assets'); ?>/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="<?php echo base_url('assets'); ?>/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,600,700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600,700&display=swap" rel="stylesheet">

    <!-- animate.css -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">

    <!-- Bootstrap core JavaScript -->
    <script src="<?php echo base_url('assets'); ?>/vendor/jquery/jquery.min.js"></script>

    <style>
        ::selection {
            background: #8D00DC;
            color: white;
        }

        :::-moz-selection {
            background: #8D00DC;
            color: white;
        }

        body {
            /* border: 1px red solid; */
            font-family: 'Poppins', sans-serif;
        }

        * {
            /* border: 1px red solid; */
        }

        .page-section {}

        #sticky-footer {
            color: #8D00DC;
            flex-shrink: none;
        }


        @media (max-width: 575.98px) {
            .timer {
                margin-top: 100px;
            }

            .text_main {
                color: #8D00DC;
                font-size: 28px;
            }

            .display-4 {
                font-size: 30px;
            }

            .waiting_img {
                position: absolute;
                display: block;
                width: 65%;
                right: 0;
                left: 69px;
                top: -60vh;
                bottom: 0;

                /* width: 100%;
                margin-top: 25%; */
            }

        }

        @media (min-width: 992px) {
            .waiting_img {
                /* position: absolute;
            display: block;
            width: 65%;
            right: 0;
            left: 0;
            top: 50vh;
            bottom: 0;
            */
                width: 100%;
                margin-top: 25%;
            }

            .text_main {
                color: #8D00DC;
            }

            .timer {
                margin-top: 35vh;
            }
        }
    </style>
</head>

<body>


    <section class="page-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 text-center" style="height:100vh;">
                    <div class="timer">
                        <div class="display-4 oops" style="font-weight:600; color:#8D00DC;">
                            Oops!
                        </div>
                        <div class="display-4" style="font-weight:600; color:#490078;">
                            <span id="days"></span>
                            <span id="hours"></span>
                            <span id="minutes"></span>
                            <span id="second"></span>
                        </div>
                        <h1 class="text_main">Wait until the time.</h1>
                    </div>
                </div>
                <div class="col-lg-6 text-center">
                    <img class="waiting_img" src="<?php echo base_url(); ?>assets/img/register_cc/waiting_register.svg" alt="">
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <footer id="sticky-footer" class="py-4">
                        <div class="container text-center">
                            <small>&copy; <?php echo date('Y'); ?> Copyright Computer Cyber</small>
                        </div>
                    </footer>
                </div>
            </div>
        </div>
    </section>



    <script src="<?php echo base_url('assets'); ?>/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- timer -->
    <script src="<?php echo base_url('assets') ?>/js/timer.js"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url('assets'); ?>/vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Contact form JavaScript -->
    <script src="<?php echo base_url('assets'); ?>/js/jqBootstrapValidation.js"></script>
    <script src="<?php echo base_url('assets'); ?>/js/contact_me.js"></script>

    <!-- Custom scripts for this template -->
    <script src="<?php echo base_url('assets'); ?>/js/agency.js"></script>
</body>

</html>